#!/usr/bin/python3

"""
Copyright (C) 2015 Corentin Bocquillon <0x539+framagit@nybble.fr>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import tkinter as tk
from tkinter import ttk
from math import sqrt

class window(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.createWidgets(master)
        self.grid()

    def createWidgets(self, master):
        tk.Label(text="a:").grid(row=0, column=0)
        self.num1 = tk.Entry()
        self.num1.grid(row=0, column=1)

        tk.Label(text="b:").grid(row=1, column=0)
        self.num2 = tk.Entry()
        self.num2.grid(row=1, column=1)

        tk.Label(text="Resultat: ").grid(row=2, column=0)
        self.resultatStr = tk.StringVar()
        self.resultat = tk.Label(textvariable = self.resultatStr)
        self.resultat.grid(row=2, column=1)

        tk.Button(master, text="a+b", command=
                  lambda: self.resultatStr.set(int(self.num1.get()) + int(self.num2.get()))).grid(row=3, column=0)
        tk.Button(master, text="a-b", command=
                  lambda: self.resultatStr.set(int(self.num1.get()) - int(self.num2.get()))).grid(row=3, column=1)
        tk.Button(master, text="a*b", command=
                  lambda: self.resultatStr.set(int(self.num1.get()) * int(self.num2.get()))).grid(row=3, column=2)
        tk.Button(master, text="a/b", command=self.divide).grid(row=3, column=3)
        tk.Button(master, text="a^2", command=
                  lambda: self.resultatStr.set(int(self.num1.get()) ** 2)).grid(row=4, column=0)
        tk.Button(master, text="a^3", command=
                  lambda: self.resultatStr.set(int(self.num1.get()) ** 3)).grid(row=4, column=1)
        tk.Button(master, text="a^b", command=
                  lambda: self.resultatStr.set(int(self.num1.get()) ** int(self.num2.get()))).grid(row=4, column=2)
        tk.Button(master, text="sqrt(a)", command=self.squareroot).grid(row=4, column=3)

    def divide(self):
        if self.num2.get() == "0":
            self.resultatStr.set("Division par zero")
        else:
            self.resultatStr.set(eval(self.num1.get()) / eval(self.num2.get()))

    def squareroot(self):
        try:
            self.resultatStr.set(sqrt(int(self.num1.get())))
        except :
            self.resultatStr.set("erreur")
        
root = tk.Tk()
window = window(master=root)
window.mainloop()
